$(document).ready(function () {

	$(".registration form").submit(function(e) {

		// var data = JSON.stringify($(this).serializeArray());
		var data = $(this).serialize();

	    $.ajax({
		   	type: "POST",
		   	url: "ajax.php",
		   	data: data,
		   	success: function(response) {  
	            var responseData = jQuery.parseJSON(response),
	            	status;

	            console.log(responseData.messageHandler);
	            switch(responseData.status) {
	            	case "error":
	            		$(".registration .info").show();
	            		$(".registration .info").removeClass("success");
	            		$(".registration .info").addClass("error");
	            		$(".registration .info span").text(responseData.message);
	            		break;
	            	case "success":
	            		$(".registration .info").show();
	            		$(".registration .info").removeClass("error");
	            		$(".registration .info").addClass("success");
	            		$(".registration .info span").text(responseData.message);
	            		break;
	            }

	            if (responseData.redirect.length > 0) {
	            	location.replace(responseData.redirect);
	            }
	        }
	 	});

	    e.preventDefault();
	});


	$(".authorization form").submit(function(e) {

		var data = $(this).serialize();

	    $.ajax({
		   	type: "POST",
		   	url: "ajax.php",
		   	data: data,
		   	success: function(response) {  
	            var responseData = jQuery.parseJSON(response),
	            	status;

	            console.log(responseData.messageHandler);
	            switch(responseData.status) {
	            	case "error":
	            		$(".authorization .info").show();
	            		$(".authorization .info").removeClass("success");
	            		$(".authorization .info").addClass("error");
	            		$(".authorization .info span").text(responseData.message);
	            		break;
	            	case "success":
	            		$(".authorization .info").show();
	            		$(".authorization .info").removeClass("error");
	            		$(".authorization .info").addClass("success");
	            		$(".authorization .info span").text(responseData.message);
	            		break;
	            }

	            if (responseData.redirect.length > 0) {
	            	location.replace(responseData.redirect);
	            }
	        }
	 	});

	    e.preventDefault();
	});

	$(".logout form").submit(function(e) {

		var data = $(this).serialize();

	    $.ajax({
		   	type: "POST",
		   	url: "ajax.php",
		   	data: data,
		   	success: function(response) {  
	            location.replace("/");
	        }
	 	});

	    e.preventDefault();
	});

});
// NgOkkm1Aeg