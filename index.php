<?php
if (!empty($_COOKIE["sid"])) {
    session_id($_COOKIE["sid"]);
}

session_start();
require_once("classes/Auth.class.php");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Auth</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<?php if (Auth\User::isAuthorized()): ?>
    <section>
        <div class="container">
            <div class="logout">
                <?php print("Привет, " . $_SESSION["user_name"] . "!"); ?>
                <form action="" method="POST">
                    <input name="action" type="hidden" value="logout">
                    <input name="submit" type="submit" class="btn" value="Выйти!">
                </form>
            </div>
        </div>
    </section>
<?php else: ?>
    <section>
        <div class="container">
            <div class="registration">
                <h4 class="title"><strong>Registration</strong></h4>
                <div class="info"><span></span></div>
                <form action="" method="POST">
                    <input name="username" type="text" class="name" placeholder="Ваш логин" value="">
                    <input name="password1" type="password" class="password" placeholder="Ваш пароль" value="">
                    <input name="password2" type="password" class="password" placeholder="Повторите пароль" value="">
                    <input name="action" type="hidden" value="registration">
                    <input name="submit" type="submit" class="btn" value="Зарегистрироваться!">
                </form>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="authorization">
                <h4 class="title"><strong>Authorization</strong></h4>
                <div class="info"><span></span></div>
                <form action="" method="POST">
                    <input name="username" type="text" class="name" placeholder="Ваш логин" value="">
                    <input name="password" type="password" class="password" placeholder="Ваш пароль" value="">
                    <input name="action" type="hidden" value="authorization">
                    <input name="submit" type="submit" class="btn" value="Авторизироваться!">
                </form>
            </div>
        </div>
    </section>
<?php endif; ?>
<script src="js/jquery.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>