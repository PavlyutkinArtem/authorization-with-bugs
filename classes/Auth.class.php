<?php

namespace Auth;

class User
{

    public $user_name;
    private $id;
    private $username;
    private $db;
    private $user_id;
    private $db_host = "";
    private $db_name = "";
    private $db_user = "";
    private $db_pass = "";

    private $is_authorized = false;

    public function __construct($username = null, $password = null)
    {
        $this->username = $username;
        $this->connectdb($this->db_name, $this->db_user, $this->db_pass, $this->db_host);
    }

    public function connectdb($db_name, $db_user, $db_pass, $db_host = "localhost")
    {
        try {
            $this->db = new \pdo("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
        } catch (\pdoexception $e) {
            echo "Ошибка базы данных: " . $e->getmessage();
            die();
        }
        $this->db->query('set names utf8');

        return $this;
    }

    public static function isAuthorized()
    {
        if (!empty($_SESSION["user_id"])) {
            return (bool)$_SESSION["user_id"];
        }
        return false;
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function logout()
    {
        if (!empty($_SESSION["user_id"])) {
            unset($_SESSION["user_id"]);
        }
    }

    public function authorize($username, $password, $remember = false)
    {

        $authHandler = new \authHandler();

        $query = "select * from users where
	            username = :username limit 1";

        $sth = $this->db->prepare($query);
        $sth->execute(
            array(
                ":username" => $username
            )
        );
        $result = $sth->fetch();

        if (password_verify($password, $result["password"])) {
            $this->is_authorized = true;
            $this->user_id = $result["id"];
            $this->user_name = $result["username"];
            $this->saveSession($remember);
            $authHandler->response("success", "Вы вошли", "", "/");
        } else {
            $this->is_authorized = false;
            $authHandler->response("error", "Неправильный логин или пароль", "", "");
        }

        // return $this->is_authorized;

    }

    public function saveSession($remember = false, $http_only = true, $days = 7)
    {
        $_SESSION["user_id"] = $this->user_id;
        $_SESSION["user_name"] = $this->user_name;

        if ($remember) {
            // Save session id in cookies
            $sid = session_id();

            $expire = time() + $days * 24 * 3600;
            $domain = ""; // default domain
            $secure = false;
            $path = "/";

            $cookie = setcookie("sid", $sid, $expire, $path, $domain, $secure, $http_only);
        }
    }

    public function create($username, $password)
    {
        $authHandler = new \authHandler();
        $user_exists = $this->checkExist($username);

        if ($user_exists) {
            $authHandler->response("error", "Пользователь уже существует: $username", "", "");
            die();
        }

        $query = "insert into users (username, password)
	            values (:username, :password)";
        $hashes = $this->passwordHash($password);
        $sth = $this->db->prepare($query);

        try {
            $this->db->beginTransaction();
            $result = $sth->execute(
                array(
                    ':username' => $username,
                    ':password' => $hashes['hash']
                )
            );
            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();

            //response
            $authHandler->response("error", "Ошибка баз данных", $e->getMessage(), "");
            die();
        }

        if (!$result) {
            $info = $sth->errorInfo();
            /// printf("Ошибка баз данных %d %s", $info[1], $info[2]);
            die();
        } else {
            $authHandler->response("success", "Пользователь создан: $username", "", "");
        }

        return $result;
    }

    public function checkExist($username)
    {

        $query = "select * from users where username = :username limit 1";

        $sth = $this->db->prepare($query);
        $sth->execute(
            array(
                ":username" => $username
            )
        );

        $row = $sth->fetch();

        if (!$row) {
            return false;
        }

        return $row["username"];
    }

    public function passwordHash($password, $salt = null, $iterations = 10)
    {
        $salt || $salt = md5(time());

        $options = [
            "salt" => $salt,
            "cost" => 12
        ];

        $hash = password_hash($password, PASSWORD_DEFAULT, $options);

        return array(
            "hash" => $hash
        );
    }
}

?>