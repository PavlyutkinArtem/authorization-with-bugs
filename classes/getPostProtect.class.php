<?php

class getPostProtect
{

    private $GET = array();
    private $POST = array();

    public function __construct()
    {
        if ($_POST) $this->postArray();
        if ($_GET) $this->getArray();
    }

    private function postArray()
    {
        foreach ($_POST as $key => $value) {
            $value = htmlentities(trim($value), ENT_QUOTES, 'utf-8');
            $this->POST[$key] = $value;
        }
    }

    private function getArray()
    {
        foreach ($_GET as $key => $value) {
            $value = htmlentities(trim($value), ENT_QUOTES, 'utf-8');
            $this->GET[$key] = $value;
        }
    }

    public function getPostObj()
    {
        settype($this->POST, 'object');
        return $this->POST;
    }

    public function getGetObj()
    {
        settype($this->GET, 'object');
        return $this->GET;
    }
}

?>