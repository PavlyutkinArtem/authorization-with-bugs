<?php
if (!empty($_COOKIE["sid"])) {
    session_id($_COOKIE["sid"]);
}

session_start();
require_once("classes/getPostProtect.class.php");
require_once("classes/Auth.class.php");

class authHandler
{

    public $data; // То, что пришло сюда

    private $action; // Действие

    public function __construct()
    {
    }

    public function action($data)
    {

        switch ($data["action"]) {
            case "registration":
                $this->registration($data);
                break;
            case "authorization":
                $this->authorization($data);
                break;
            case "logout":
                $this->logout();
                break;
        }

    }

    public function registration($data)
    {

        setcookie("sid", "");

        $username = $data["username"];
        $password1 = $data["password1"];
        $password2 = $data["password2"];

        $user = new Auth\User();

        if (empty($username)) {
            $this->response("error", "Введите, пожалуйста, ваш логин!", "", "");
        } elseif (!preg_match('/^[a-z\d]{4,64}$/i', $username)) {
            $this->response("error", "Ваш логин не валидный!", "", "");
        } elseif (empty($password1)) {
            $this->response("error", "Введите, пожалуйста, ваш пароль!", "", "");
        } elseif (strlen($password1) < 6) {
            $this->response("error", "Ваш пароль слишком короткий!", "", "");
        } elseif (strlen($password1) > 64) {
            $this->response("error", "Ваш пароль слишком длинный!", "", "");
        } elseif (!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/', $password1)) {
            $this->response("error", "Ваш пароль не валидный!  $password1", "", "");
        } elseif (empty($password2)) {
            $this->response("error", "Повторите, пожалуйста, ваш пароль!", "", "");
        } elseif (strlen($password2) < 6) {
            $this->response("error", "Ваш пароль повторный слишком короткий!", "", "");
        } elseif (strlen($password2) > 64) {
            $this->response("error", "Ваш пароль повторный слишком длинный!", "", "");
        } elseif (!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/', $password1)) {
            $this->response("error", "Ваш пароль не валидный!  $password1", "", "");
        } elseif ($password1 !== $password2) {
            $this->response("error", "Упс, пароли не совпадают!", "", "");
        } else {
            $new_user_id = $user->create($username, $password1);
        }

        $user->authorize($username, $password1);
    }

    public function response($status, $message, $messageHandler, $redirect)
    {

        $response = array(
            "status" => $status,
            "message" => $message,
            "messageHandler" => $messageHandler,
            "redirect" => $redirect
        );

        echo json_encode($response);
        die();

    }

    public function authorization($data)
    {

        setcookie("sid", "");

        $username = $data["username"];
        $password = $data["password"];

        $user = new Auth\User();

        if (empty($username)) {
            $this->response("error", "Введите, пожалуйста, ваш логин!", "", "");
        } elseif (!preg_match('/^[a-z\d]{4,64}$/i', $username)) {
            $this->response("error", "Ваш логин не валидный!", "", "");
        } elseif (empty($password)) {
            $this->response("error", "Введите, пожалуйста, ваш пароль!", "", "");
        } elseif (strlen($password) < 6) {
            $this->response("error", "Ваш пароль слишком короткий!", "", "");
        } elseif (strlen($password) > 64) {
            $this->response("error", "Ваш пароль слишком длинный!", "", "");
        } elseif (!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/', $password)) {
            $this->response("error", "Ваш пароль не валидный!  $password1", "", "");
        } else {
            $user->authorize($username, $password);
        }
    }

    public function logout()
    {
        $user = new Auth\User();
        $user->logout();

    }

}

$array = array();
foreach ($_POST as $key => $value) {
    $array[$key] = $value;
}

//getPostProtect
// $getPostProtect = new getPostProtect();
$authHandler = new authHandler();
$authHandler->action($array);

?>